import os
import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
import cartopy
import time

FigPath="/home/fykej/yukon-freeze-thaw-storage-bucket/figs/"
DataPath="/home/fykej/"
m="CanESM2"
s="85"
d="BCCAQ"

plotSeasonCycle=False
plotSpatialChange=True

def load_data(DataPath,m,d,v):
    f=DataPath+m+"_"+s+"_"+d+"_"+v+".nc"
    df=xr.open_dataset(f)
    t_h=df[v].loc[str(1980)+"-01-01":str(2010)+"-12-31",:,:]
    t_f=df[v].loc[str(2070)+"-01-01":str(2100)+"-12-31",:,:]
    return t_h,t_f
print('Starting data load')
start=time.time()
tmin_h,tmin_f=load_data(DataPath,m,d,"tasmin")
tmax_h,tmax_f=load_data(DataPath,m,d,"tasmax")
stop=time.time()
print('Time elapsed: '+str(stop-start))

print('Starting freeze/thaw calculation')
start=time.time()

def calculate_freeze_thaw(tmin,tmax,method):
    if method=="pm2CAnnualCount":
        i=np.where((tmin<-2.) & (tmax>2.))
        arr=np.zeros(np.shape(tmin))
        arr[i]=1
        ft=np.sum(arr,axis=0)
        del arr,i
    if method=="pm2CDJFCount":
        iDJF=tmax.groupby('time.season').groups['DJF']
        tmax=tmax[iDJF,:,:]
        tmin=tmin[iDJF,:,:]
        i=np.where((tmin<-2.) & (tmax>2.))
        arr=np.zeros(np.shape(tmax))
        arr[i]=1
        ft=np.sum(arr,axis=0)
        del arr,i
    return ft


method="pm2CDJFCount"
ft_h=calculate_freeze_thaw(tmin=tmin_h,tmax=tmax_h,method=method)
ft_f=calculate_freeze_thaw(tmin=tmin_f,tmax=tmax_f,method=method)
stop=time.time()
print('Time elapsed: '+str(stop-start))

print('Starting difference calculation and final xarray array generation')
start=time.time()
#d_ft_np=(ft_f-ft_h)/ft_h*100.
d_ft_np=(ft_f-ft_h)/30.#divide by climatological period length, to get average increase per year
lat=tmin_h.coords['lat']
lon=tmin_h.coords['lon']
d_ft=xr.DataArray(d_ft_np,coords=[('lat',lat),('lon',lon)])
stop=time.time()
print('Time elapsed: '+str(stop-start))

if plotSpatialChange:
    print('Generating spatial plot')
    start=time.time()
    ax = plt.axes(projection=cartopy.crs.Orthographic(-125, 60))
    img=d_ft.plot.pcolormesh(ax=ax,transform=cartopy.crs.PlateCarree(),cmap='bwr')
    img.colorbar.set_label("Mean change in number of DJF freeze/thaw days\n(1980-2010 to 2070-2100)")
    states_provinces = cartopy.feature.NaturalEarthFeature(
            category='cultural',
            name='admin_1_states_provinces_lines',
            scale='50m',
            facecolor='none')
    countries = cartopy.feature.NaturalEarthFeature(
            category='cultural',
            name='admin_0_boundary_lines_land',
            scale='50m',
            facecolor='none')
    ax.add_feature(countries,edgecolor='black')
    ax.add_feature(states_provinces,edgecolor='black')
    lat=60.72
    lon=-135.06
    ax.scatter(lon,lat,color='black',marker='o',transform=cartopy.crs.PlateCarree())
    ax.text(lon+0.5,lat,"Whitehorse",transform=cartopy.crs.PlateCarree())
    ax.coastlines()
    plt.savefig(FigPath+"Spatial_m2_to_p2_freeze_thaw.png",dpi=300)
    plt.close("all")
    stop=time.time()
    print('Time elapsed: '+str(stop-start))

if plotSeasonCycle:
    print('Plotting representative seasonal cycles')

    def monthly_climo(tasmin,tasmax):
        tasmean=(tasmin+tasmax)/2
        m_mean_min=np.zeros(52)
        m_mean_max=np.zeros(52)
        m_mean_mean=np.zeros(52)
        for m in range(52):
            i=tasmin.groupby('time.week').groups[m+1]
            m_mean_mean[m]=tasmean[i].mean()
            m_mean_min[m]=tasmin[i].mean()
            m_mean_max[m]=tasmax[i].mean()
        return m_mean_mean,m_mean_min,m_mean_max

    ind=np.unravel_index(np.argmin(d_ft,axis=None),d_ft.shape)
    m_mean_mean_h,m_mean_min_h,m_mean_max_h=monthly_climo(tmin_h[:,ind[0],ind[1]],tmax_h[:,ind[0],ind[1]])
    m_mean_mean_f,m_mean_min_f,m_mean_max_f=monthly_climo(tmin_f[:,ind[0],ind[1]],tmax_f[:,ind[0],ind[1]])

    plt.axhline(linewidth=2,color='k')

    plt.plot(m_mean_mean_h,color='blue',linewidth=1)
    plt.plot(m_mean_min_h,color='blue',linewidth=2)
    plt.plot(m_mean_max_h,color='blue',linewidth=2)
    isrt=np.argsort(np.abs(m_mean_mean_h))
    plt.plot((isrt[0],isrt[0]),(m_mean_min_h[isrt[0]],m_mean_max_h[isrt[0]]),color='black')
    plt.plot((isrt[1],isrt[1]),(m_mean_min_h[isrt[1]],m_mean_max_h[isrt[1]]),color='black')

    plt.plot(m_mean_mean_f,color='red',linewidth=1)
    plt.plot(m_mean_min_f,color='red',linewidth=2)
    plt.plot(m_mean_max_f,color='red',linewidth=2)
    isrt=np.argsort(np.abs(m_mean_mean_f))
    plt.plot((isrt[0],isrt[0]),(m_mean_min_f[isrt[0]],m_mean_max_f[isrt[0]]),color='black')
    plt.plot((isrt[1],isrt[1]),(m_mean_min_f[isrt[1]],m_mean_max_f[isrt[1]]),color='black')

    plt.xlabel("Week of year")
    plt.ylabel("Temperature (C)")
    #
    plt.savefig(FigPath+"Freeze_thaw_change_minus_to_plus_2.png",dpi=300)


